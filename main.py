from planner import Planner
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="trainer")
    parser.add_argument(
        "--domain_file",
        type=str,
        default="test_problems/domain.pddl",
        help="Path to the domain pddl file"
    )
    parser.add_argument(
        "--problem_file",
        type=str,
        default="test_problems/simple_problem.pddl",
        help="Path to the problem pddl file"
    )
    parser.add_argument(
        "--weight",
        type=int,
        default=20,
        help="Weight for the weighted A* algorithm"
    )
    parser.add_argument(
        "--print_plan",
        type=bool,
        default=True,
        help="If True, print plan"
    )
    args = parser.parse_args()
    planner = Planner(args.domain_file, args.problem_file, args.weight)
    plan = planner.solve(return_plan=args.print_plan)
    for action in plan:
        print(action)