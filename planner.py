import time
from pddlpy import DomainProblem
import heapq

class Planner:
    def __init__(self, domainfile, problemfile, weight:int = 20):
        self.domprob = DomainProblem(domainfile, problemfile)
        self._initial_state = self._to_set_of_tuples(self.domprob.initialstate())
        self._goal = self._to_set_of_tuples(self.domprob.goals())
        self.weight = weight

    def applicable(self, state, action):
        """
        action: operator
        """
        # Get the preconditions of the action
        preconditions_pos = action.precondition_pos
        preconditions_neg = action.precondition_neg

        # Check if the action is applicable
        return all(pre in state for pre in preconditions_pos) and not any(pre in state for pre in preconditions_neg)
    
    def progression(self, state, action):
        """
        action: operator
        """
        # Check if the action is applicable
        if self.applicable(state, action):
            new_state = state.copy()
            for effect in action.effect_pos:
                new_state.add(effect)
            for effect in action.effect_neg:
                new_state.discard(effect)
            return new_state
        else:
            print(f"Action {(action.operator_name, action.variable_list)} is not applicable in state {state}")
            return state

    def is_plan_valid(self, plan):
        # Check if the plan is valid from the given state
        state = self._initial_state
        for action in plan:
            state = self.progression(state, action)
        return all(subgoal in state for subgoal in self._goal)

    def count_actions(self, plan):
        return len(plan)
    
    def applicable_actions(self, state):
        action_list = []
        for operator in self.domprob.operators():
            for gop in self.domprob.ground_operator(operator):
                if self.applicable(state, gop):
                    action_list.append(gop)
        return action_list

    def heuristic(self, state):
        satisfied_subgoals = 0
        for l in state:
            if l in self._goal:
                satisfied_subgoals += 1
        return len(self._goal) - satisfied_subgoals

    def produce_plan(self):
        # A* search algorithm
        open_list = [(self.heuristic(self._initial_state), self._initial_state, [])]
        closed_list = set()

        while open_list:
            _, state, plan = heapq.heappop(open_list)

            if self._goal.issubset(state):
                return plan

            closed_list.add(frozenset(state))

            for action in self.applicable_actions(state):
                new_state = self.progression(state, action)
                new_plan = plan + [action]

                if new_state in closed_list:
                    continue

                cost = len(new_plan) + self.weight * self.heuristic(new_state)

                for _, existing_state, existing_plan in open_list:
                    if existing_state == new_state and len(existing_plan) <= len(new_plan):
                        break
                else:
                    heapq.heappush(open_list, (cost, new_state, new_plan))

        return None  # No plan found
    
    def solve(self, return_plan=False):
        print("Solving...")
        start_time = time.time()
        plan = self.produce_plan()
        end_time = time.time()

        if plan is None:
            print("No plan found")
        else:
            num_steps = self.count_actions(plan)
            execution_time = end_time - start_time
            print(f"Plan found with {num_steps} steps. Execution time: {execution_time} seconds.")
            if return_plan:
                return [(action.operator_name, action.variable_list) for action in plan]

    @staticmethod
    def _to_set_of_tuples(state):
        """
        state: list of atoms
        """
        set_of_tuples = set()
        for atom in state:
            tup = tuple(atom.predicate)
            set_of_tuples.add(tup)
        return set_of_tuples
    
    @property
    def initial_state(self):
        return self._initial_state

    @property
    def goal(self):
        return self._goal