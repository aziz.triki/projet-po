(define (problem problem_logistics)
(:domain basic_logistics)
(:requirements :strips :typing)

    (:objects 
        wp1 wp2 wp3 - location
        t1 - truck
        dr1 - driver
        pack1 - package
    )
    
    (:init
        ;; drivers
        (at dr1 wp1)
        
        ;; trucks
        (at t1 wp2)
        
        ;; packages
        (at pack1 wp1)
        
        ;; Ground Connections
        (connected wp1 wp2)
        (connected wp2 wp1)
        (connected wp2 wp3)
        (connected wp3 wp2)
    )
    
    (:goal (and 
        ;; drivers home
        (at dr1 wp1)
        
        ;; packages delivered
        (at pack1 wp3)
    ))
)