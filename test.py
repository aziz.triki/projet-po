from planner import *

planner = Planner("domain.pddl", "simple problem.pddl")
plan = planner.solve(return_plan=True)
for action in plan:
    print(action)