# Projet PO
Groupe 8
- Mohamed Aziz Triki
- Lotfi Kacha
- Mohamed Amine Ben Salha
# Install Requirements:
```
pip install pddlpy
```

# Usage
To solve a problem, run :
```
python main.py --domain_file test_problems/domain.pddl --problem_file test_problems/simple_problem.pddl --weight 20 --print_plan True
```

This code will print the plan, its length and the runtime.